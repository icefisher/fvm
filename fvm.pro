TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lpetsc
QMAKE_CFLAGS += -std=c99 -Wpedantic
QMAKE_CFLAGS_DEBUG += -pg
QMAKE_LFLAGS_DEBUG += -pg
INCLUDEPATH += /usr/include/petsc /usr/include/openmpi
SOURCES +=   src/geometry.c  src/main.c  src/mesh.c  src/solver.c \
    src/blas_simple.c \
    src/blas_petsc.c \
    src/edges.c
HEADERS  += src/blas.h    src/geometry.h  src/mesh.h  src/solver.h \
    src/edges.h
