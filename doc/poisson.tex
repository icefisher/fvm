\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T2A]{fontenc}
\usepackage[russian]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{indentfirst}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{caption}
\usepackage{bm}
\usepackage{tikz}
\usetikzlibrary{arrows}

\columnsep=0.5cm
\captionsetup{singlelinecheck=false, justification=centering}

\newcommand{\grad}{\mathop{\rm grad}\nolimits}
\renewcommand{\div}{\mathop{\rm div}\nolimits}
\newcommand{\erf}{\mathop{\rm erf}\nolimits}
\newcommand{\erfc}{\mathop{\rm erfc}\nolimits}
\newcommand{\const}{\mathop{\rm const}\nolimits}

\graphicspath{
	{./pics/}
} % папки с рисунками


\author{Борисов В. С.}
\date{}
\title{
Численное решение уравнения Пуассона методом конечных объемов
}
\begin{document}
\maketitle
\abstract{
В работе представлено численное решение двумерного уравнения Пуассона методом конечных объемов на неструктурированной сетке.

\textbf{Ключевые слова:} \textit{уравнение Пуассона, метод конечных объемов, неструктурированная сетка}.
}

\section{Постановка задачи}
Рассмотрим уравнение Пуассона в двумерной области $\Omega$
\begin{equation}
-\nabla\cdot(\nabla u ) = f(\bm x), \quad \bm x \in \Omega,
\label{eq:poisson}
\end{equation}
где $u = u(\bm x)$ -- неизвестная, и $\bm x = (x, y)$ -- координаты точки.
На границе $\Gamma = \Gamma_D \cup \Gamma_N$ выделим участок с условием Дирихле и участок с условие Неймана
\begin{equation}
u = u_D(\bm x), \quad \bm x \in \Gamma_D,
\end{equation}
\begin{equation}
\frac{\partial u}{\partial \bm n} = u_N(\bm x), \quad \bm x \in \Gamma_N,
\end{equation}
где $\bm n$ -- нормаль.

\section{Аппроксимация}
Проведем дискретизацию методом конечных объемов. Для этого из области $\Omega$ сделаем расчетную сетку, где все ячейки сетки состоят из выпуклых четырехугольников.

На рис. \ref{fg:cell} представлена ячейка $\Omega_c$ с соседними ячейками $\Omega_w$, $\Omega_n$, $\Omega_e$, $\Omega_s$. Обозначим значения неизвестной $u$ в центре каждой ячейки как $u_c, u_w, u_n, u_e, u_s$. Смежные ребра от ячейки $\Omega_c$ к каждой соседней ячейке обозначим как $\Gamma_w$, $\Gamma_n$, $\Gamma_e$, $\Gamma_s$, границу ячейки $\Omega_c$ обозначим как $\Gamma_c$.

\begin{figure}
\begin{center}
\begin{tikzpicture}[scale=0.5]
%square
%\path [fill=lightgray] (0,0)--(0,5)--(5,5)--(5,0)--(0,0);
\draw (0, 3) -- (11, 4);
\draw (0, 7) -- (11, 6);
\draw (4, 0) -- (3, 10);
\draw (7, 0) -- (8, 10);

\draw (0, 3) -- (0, 7);
\draw (4, 0) -- (7, 0);
\draw (3, 10) -- (8, 10);
\draw (11, 4) -- (11, 6);

%labels
\node at (2, 5) {$\bullet$}; 
\node at (2, 5) [above] {w};
\node [gray] at (1, 4) {$\Omega_w$};
\node [gray] at (2.9, 6) {$\Gamma_w$};

\node at (9.2, 5) {$\bullet$}; 
\node at (9.2, 5) [above] {e};
\node [gray] at (10.5, 4.5) {$\Omega_e$};
\node [gray] at (8, 5) {$\Gamma_e$};

\node at (5.5, 8.5) {$\bullet$};  
\node at (5.5, 8.5) [above] {n};
\node [gray] at (7, 9.5) {$\Omega_n$};
\node [gray] at (5.5, 7) {$\Gamma_n$};

\node at (5.5, 2) {$\bullet$}; 
\node at (5.5, 2) [above] {s};
\node [gray] at (5, 0.5) {$\Omega_s$};
\node [gray] at (6.8, 3) {$\Gamma_s$};

\node at (5.5, 5) {$\bullet$}; 
\node at (5.5, 5) [above] {c};
\node [gray] at (4.5, 4) {$\Omega_c$};

\end{tikzpicture}
\end{center}
\caption{Ячейка $\Omega_c$ и ее соседи.}
\label{fg:cell}
\end{figure}

Возьмем интеграл от уравнения (\ref{eq:poisson}) по ячейке $\Omega_c$
\begin{equation}
- \int_{\Omega_c} \nabla\cdot(\nabla u) \, d\bm x = \int_{\Omega_c} f \, d\bm x.
\end{equation}

Используя теорему о дивергенции, преобразуем интеграл по площади в интеграл по границе
\begin{equation}
- \int_{\Gamma_c} \nabla u \cdot \bm n \, ds = \int_{\Omega_c} f \, d\bm x.
\end{equation}

Воспользуемся теоремой о среднем и преобразуем правую часть уравнения
\begin{equation}
- \int_{\Gamma_c} \nabla u \cdot \bm n \, ds = \overline{f} \cdot |\Omega_c|,
\end{equation}
где $\overline{f}$ -- среднее значение функции $f$ в ячейке $\Omega_c$, $|\Omega_c|$ -- площадь ячейки.

Преобразуем интеграл по границе в сумму интегралов по граням ячейки
\begin{equation}
- \sum_{i \in w, n, e, s} \int_{\Gamma_i} \frac{\partial u}{\partial \bm n} \, ds = \overline{f} \cdot |\Omega_c|.
\end{equation}

Воспользуемся теоремой о среднем и преобразуем левую часть
\begin{equation}
- \sum_{i \in w, n, e, s} \overline{\frac{\partial u}{\partial \bm n}} \cdot |\Gamma_i| = \overline{f} \cdot |\Omega_c|,
\label{eq:meaned}
\end{equation}
где $\overline{\frac{\partial u}{\partial \bm n}}$ -- среднее значение производной по нормали на границе $\Gamma_i$, $|\Gamma_i|$ -- длина границы $\Gamma_i$.

Аппроксимируем производную по нормали центральной разностной схемой
\begin{equation}
\overline{\frac{\partial u}{\partial \bm n}} \approx \frac{u_i - u_c}{|ic|}, \quad i \in w, n, e, s,
\end{equation}
где $|ic|$ -- длина отрезка от точки $i$ до точки $c$. 

В качестве среднего $\overline{f}$ возьмем значение в центральной точке $c$
\begin{equation}
\overline{f} \approx f(c).
\end{equation}

Подставив аппроксимации в (\ref{eq:meaned}) получим линейное уравнение
\begin{equation}
- \sum_{i \in w, n, e, s} \frac{u_i - u_c}{|ic|} |\Gamma_i| = f(c) \cdot |\Omega_c|.
\end{equation}

Если какая-либо граница $\Gamma_i$ ячейки $\Omega_c$ расположена на границе  Дирихле $\Gamma_D$, например на рис. \ref{fg:cell-boundary}, то производная по нормали аппроксимируется разностной схемой
\begin{equation}
\overline{ \frac{\partial u}{\partial \bm n} } \approx \frac{u_b - u_c}{|bc|},
\end{equation}
где $b$ -- точка на середине границы $\Gamma_i$ и $u_b$ -- значение известной функции $u_D$ в этой точке.

\begin{figure}
\begin{center}
\begin{tikzpicture}[scale=0.5]
%square
%\path [fill=lightgray] (0,0)--(0,5)--(5,5)--(5,0)--(0,0);
\draw (3.7, 3) -- (11, 4);
\draw (3.3, 7) -- (11, 6);
\draw [red] (4, 0) -- (3, 10);
\draw (7, 0) -- (8, 10);

\draw (4, 0) -- (7, 0);
\draw (3, 10) -- (8, 10);
\draw (11, 4) -- (11, 6);

%labels
\node at (3.5, 5) {$\bullet$}; 
\node at (3.5, 5) [left] {b};
\node [red] at (2, 7) {$\Gamma_D$};
\node [gray] at (3, 4) {$\Gamma_w$};

\node at (9.2, 5) {$\bullet$}; 
\node at (9.2, 5) [above] {e};
\node [gray] at (10.5, 4.5) {$\Omega_e$};
\node [gray] at (8, 5) {$\Gamma_e$};

\node at (5.5, 8.5) {$\bullet$};  
\node at (5.5, 8.5) [above] {n};
\node [gray] at (7, 9.5) {$\Omega_n$};
\node [gray] at (5.5, 7) {$\Gamma_n$};

\node at (5.5, 2) {$\bullet$}; 
\node at (5.5, 2) [above] {s};
\node [gray] at (5, 0.5) {$\Omega_s$};
\node [gray] at (6.8, 3) {$\Gamma_s$};

\node at (5.5, 5) {$\bullet$}; 
\node [gray] at (5.5, 5) [above] {c};
\node [gray] at (4.5, 4) {$\Omega_c$};

\end{tikzpicture}
\end{center}
\caption{Ячейка $\Omega_c$ на границе.}
\label{fg:cell-boundary}
\end{figure}

Если какая-либо граница $\Gamma_i$ ячейки $\Omega_c$ расположена на границе  Неймана $\Gamma_N$, то производная по нормали аппроксимируется как значение известной функции $u_N$ в точке $b$.
\begin{equation}
\overline{ \frac{\partial u}{\partial \bm n} } \approx u_N.
\end{equation}

Система линейных уравнений получается путем аппроксимации интеграла для каждой ячейки расчетной сетки. Заметим, что на структурированной сетке с равномерным шагом система уравнений аналогична полученной через разностную схему крест.

\section{Реализация}
Решатель написан на языке программирования \texttt{C} и состоит из следующих частей для работы с: сеткой, вычислительной геометрией, построением задачи, линейными решателями.

\paragraph{Сетка.}
В модуле реализовано считывание созданной в программе \texttt{Gmsh}\footnote{http://gmsh.info/} сетки в формате \texttt{MeshFormat}.
Поддерживаются метки типа \texttt{Physical Surface} для указания граничных условий.
Также, присутствует генератор неструктурированной сетки из единичного квадрата. Решатель вычисляет значение неизвестной для центральной точки в каждой ячейке, но для наглядной визуализации с помощью программы \texttt{ParaView}\footnote{http://www.paraview.org/} решение должно быть найдено в каждом узле сетки. Поэтому значения в точках сетки вычисляются как среднее арифметическое от значений центральных точек в соседних ячейках. 
Вычисленные решения в ячейках и узлах записываются в виде текстового \texttt{VTK}\footnote{http://www.vtk.org/VTK/img/file-formats.pdf} файла.

\paragraph{Вычислительная геометрия.}
Модуль для работы с геометрией определяет основные структуры данных для работы с сеткой: точка, отрезок, четырехугольная ячейка, вектор.
И основные функции для работы с этим геометрическими объектами, например вычисление площади ячейки, поиск соседних ячеек и соединяющих их отрезков.

\paragraph{Линейный решатель.}
Модуль содержит функции для работы с матрицами и векторами: создание, уничтожение, копирование, установка значения по индексу, взятие значения по индексу.
Были реализованы простейшие решатели СЛАУ методами простой итерации, Гаусса-Зейделя и верхней релаксации.
В дальнейшем, были использованы программные библиотеки \texttt{LIS}\footnote{http://www.ssisc.org/lis/} и \texttt{PETSc}\footnote{http://www.mcs.anl.gov/petsc/}, и заменены реализации структур и функций с сохранением интерфейса модуля.
Использованные библиотеки, в перспективе, позволяют распараллелить вычисление линейной системы и адаптировать решатель к выполнению на высокопроизводительных вычислительных системах.

\paragraph{Построитель задачи.}
Линейное уравнение, полученное для одной ячейки, записывается для всех ячеек из сетки. Необходимо, правильно выполнить индексацию и сформировать матрицу и вектор правой части для СЛАУ.
При этом учитывать граничные условия для краевых ячеек.
То есть, этот модуль занимается построением СЛАУ с использованием функций из модуля Линейного решателя, и данный процесс не зависит от конкретной реализации модуля Линейного решателя.

\section{Численные примеры}
Решатель был проверен на искусственном аналитическом решении
\begin{equation}
u_{ex} = \sin (Ax) \cos(By), \quad \bm x \in \Omega \cup \Gamma,
\end{equation}
где $\Omega$ внутренность единичного квадрата и $\Gamma$ периметр этого квадрата.
Подставив решение в уравнение (\ref{eq:poisson}) найдем правую часть
\begin{equation}
f = (A^2 + B^2) \sin (Ax) \cos(By), \quad \bm x \in \Omega.
\end{equation}
Граничные условия для Дирихле возьмем как аналитическое решение
\begin{equation}
u_D = u_{ex}, \quad \bm x \in \Gamma.
\end{equation}

Для решения линейной системы использовался методов подпространств Крылов из библиотеки PETSc. Использовался метод GMRES с предобуславливателем LU, итерации выполняются до достижения точности в $10^{-10}$.

В анализе результатов расчетов использовались два показателя ошибки: норма погрешности $L_2$ и максимальная невязка.
Вычисление максимальной невязка получается взятием максимального модуля невязки во всех центрах ячеек
\begin{equation}
\varepsilon_{max} = \max_{\Omega_c \in \Omega} |u_{num} (c) - u_{ex} (c) |,
\end{equation}
где $u_{num}(c)$ -- это значение численного решение $u_num$ в центре $c$ ячейки $\Omega_c$.

В дискретном случае, вычисляем норму погрешности $L_2$ как корень суммы произведения площадей ячеек на квадраты разностей значений функций в центрах ячеек
\begin{equation}
\varepsilon_{L_2} = \left( \sum_{\Omega_c \in \Omega} |\Omega_c|\left( u_{num} (c) - u_{ex} \left(c\right) \right)^2  \right)^{\frac{1}{2}}.
\end{equation}

Порядок аппроксимации $n$ определяется из соотношения
\begin{equation}
\varepsilon = Ch^{n},
\end{equation}
где $\varepsilon$ -- погрешность, $C$ -- константа.

\begin{table}
\begin{center}
\begin{tabular}{l|c|c|c|c|c|c}
\hline
$h$	&	$\varepsilon_{L_2}$	&	$O(\varepsilon_{L_2})$	&	$\varepsilon_{max}$	&	$O(\varepsilon_{max})$	\\
\hline
1/2	&	3.566176	&		&	1.175000	&		\\
1/4	&	0.373677	&	3.254	&	0.680419	&	0.788	\\
1/8	&	0.076759	&	2.283	&	0.162041	&	2.070	\\
1/16	&	0.018360	&	2.063	&	0.043621	&	1.893	\\
1/32	&	0.004545	&	2.014	&	0.011499	&	1.925	\\
1/64	&	0.001134	&	2.002	&	0.002972	&	1.952	\\
1/128	&	0.000283	&	2.002	&	0.000753	&	1.987	\\
1/256	&	0.000071	&	1.994	&	0.000190	&	1.986	\\
1/512	&	0.000018	&	1.979	&	0.000048	&	1.988	\\
\hline 
\end{tabular} 
\caption{Погрешности вычисления в \\зависимости от размера ячейки.}
\label{tbl:errors}
\end{center}
\end{table}

В табл. \ref{tbl:errors} показана норма погрешности $L_2$ и порядок аппроксимации решений для $A=B=10$. 
При построении сетки используется параметр $h$ -- размер ребра ячейки.
Для определения сходимости по сетке были проведены расчеты для сеток с $h = 1/2$, $1/4$, $1/8$, ..., $1/512$.
Погрешность уменьшается пропорционально размеру ячейки с вторым порядком аппроксимации.


\end{document}