#ifndef EDGES_H
#define EDGES_H

#include "geometry.h"

void edges_add(LINE line, CELL *cell);
int edges_get(LINE line, CELL *cells[2]);

#endif // EDGES_H
