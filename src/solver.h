#pragma once

#include "mesh.h"
#include "geometry.h"
#include "blas.h"

typedef struct {
    Mesh mesh;
    MATRIX* A;
    VECTOR* b;

    int print_norm;
    int max_iteration;
    double sor_omega;
    double tolerance;
} LINEAR_SYSTEM;

typedef enum {
    SOLVER_ZEIDEL,
    SOLVER_SOR,
    SOLVER_LI
} SOLVER_METHOD;

typedef enum {
    BC_DIRICHLET,
    BC_NEUMANN
} BC_TYPE;

typedef struct {
    double (*fun)(POINT p);
    int mark;
    BC_TYPE type;
} BOUNDARY_CONDITION;

LINEAR_SYSTEM linsys_create_poisson(Mesh mesh,
                                    double lambda,
                                    double (*f)(POINT),
                                    BOUNDARY_CONDITION* bcs,
                                    int nbcs);
LINEAR_SYSTEM linsys_create_unstationary_poisson(Mesh mesh, VECTOR* x0, double tau,
                                         double lambda, double (*f)(POINT), BOUNDARY_CONDITION* bcs, int nbcs);
LINEAR_SYSTEM linsys_create_stokes(Mesh mesh);
LINEAR_SYSTEM linsys_poisson2(Mesh mesh, double (*f)(POINT), BOUNDARY_CONDITION *bcs, int nbcs);
void linsys_destroy(LINEAR_SYSTEM ls);
