#include "edges.h"
#include <assert.h>

struct CELL_NODE {
    CELL* cell;
    struct CELL_NODE* next;
};

struct LINE_NODE {
    LINE line;
    struct CELL_NODE *cells;
    struct LINE_NODE *next;
};

typedef struct CELL_NODE CELL_NODE;
typedef struct LINE_NODE LINE_NODE;

#define HASH_SIZE (1024*1024)
static LINE_NODE* nodes[HASH_SIZE];

int hash(LINE line) {
    return (line.a->id + line.b->id) % HASH_SIZE;
//    return 0;
}

CELL_NODE* add_cell(CELL_NODE *nodes, CELL *cell) {
    CELL_NODE *c = malloc(sizeof(CELL_NODE));
    c->cell = cell;
    c->next = nodes;
    return c;
}

void edges_add(LINE line, CELL *cell) {
    int h = hash(line);
    LINE_NODE* ln = nodes[h];
    LINE_NODE* prev = 0;
    while(ln && !line_equals(ln->line, line)) {
        prev = ln;
        ln = ln->next;
    }
    if (!ln) {
        ln = malloc(sizeof(LINE_NODE));
        ln->line = line;
        ln->next = 0;
        ln->cells = 0;
        if (prev) {
            prev->next = ln;
        } else {
            nodes[h] = ln;
        }
    }
    ln->cells = add_cell(ln->cells, cell);
}


int edges_get(LINE line, CELL *cells[2]) {
    int h = hash(line);
    LINE_NODE* ln = nodes[h];
    while(ln && !line_equals(ln->line, line)) {
        ln = ln->next;
    }
    if (ln == 0) {
        return 0;
    }
    int k = 0;
    CELL_NODE *cn = ln->cells;
    while(cn) {
        assert(k < 2);
        cells[k] = cn->cell;
        cn = cn->next;
        k++;
    }
    return k;
}
