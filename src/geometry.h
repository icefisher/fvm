#pragma once

typedef struct POINT {
	double x;
	double y;
	int id;
} POINT;

typedef struct LINE {
    POINT* a;
    POINT* b;
    int mark;
} LINE;

typedef struct CELL {
    POINT *points[4];
	int id;
    int lines_marks[4];
} CELL;

typedef struct VEC2D {
	double x;
	double y;
} VEC2D;

POINT point_create(double x, double y, int id);
double point_distance(POINT p1, POINT p2);
double point_line_distance(POINT p, LINE l);

LINE line_create(POINT *p1, POINT *p2);
double line_length(LINE l);
double line_angle(LINE l1, LINE l2);
POINT line_center_point(LINE l);
int line_is_valid(LINE l);
int line_equals(LINE l1, LINE l2);
void line_print(LINE l);

CELL cell_create(POINT *p1, POINT *p2, POINT *p3, POINT *p4, int id);
POINT cell_center_point(CELL cell);
double cell_area(CELL c);
int cell_is_valid(CELL l);
LINE cell_common_line(CELL c1, CELL c2);
int cell_has_point(CELL c, POINT *p);
int cell_has_line(CELL cell, LINE line);
void cell_print(CELL c);
void cell_get_edges(CELL c, LINE edges[4]);

VEC2D vec2d_create(POINT from, POINT to);
double vec2d_scalar(VEC2D a, VEC2D b);
double vec2d_length(VEC2D a);
void vec2d_print(VEC2D v);
