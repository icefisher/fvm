#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "blas.h"
#include "geometry.h"
#include "mesh.h"
#include "solver.h"

#define AAA 10
#define BBB 10

#ifdef USING_PETSC
    #include <petsc.h>
#endif

double f(POINT p) {
	return (AAA*AAA + BBB*BBB)*sin(AAA*p.x)*cos(BBB*p.y);
}

double bval(POINT p) {
	return sin(AAA*p.x)*cos(BBB*p.y);
}

double f0(POINT p) {
    return 0;
}

double bval1(POINT p) {
    return 1;
}

double bval2(POINT p) {
    return 2;
}

double bval3(POINT p) {
    return 3;
}

double bval4(POINT p) {
    return 4;
}


void sigsegv_handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

int poisson_dt(int argc, char* argv[]) {
    int size = 5;
    if (argc > 1)
        size = atoi(argv[1]);

//    Mesh mesh = mesh_create_from_file("../../Repos/fvm/data/complex2.msh");
    Mesh mesh = mesh_create_unit_square(size, 0.0);

    BOUNDARY_CONDITION bcs[] = {
        {bval1, 1, BC_DIRICHLET},
        {bval2, 2, BC_DIRICHLET},
        {bval3, 3, BC_DIRICHLET},
        {bval4, 4, BC_DIRICHLET},
    };

    char buf[100];
    VECTOR* x0 = vector_create(mesh.ncells);
    VECTOR* x = vector_create(mesh.ncells);
    double tau = 1;
    double t = tau;
    int iter = 0;

    while(t < 30) {
        LINEAR_SYSTEM ls = linsys_create_unstationary_poisson(mesh, x0, tau, 0.005, f0, bcs, 2);
        blas_solve(ls.A, ls.b, x);
        sprintf(buf, "out_%d.vtk", iter++);
        mesh_write_result_file(mesh, x, buf);
        linsys_destroy(ls);
        t += tau;
        vector_copy(x, x0);
        printf("solved t=%lf\n", t);
    }
    vector_destroy(x);
    vector_destroy(x0);
    mesh_destroy(mesh);

    return 0;
}

int poisson(int argc, char* argv[]) {
    int size = 5;
    if (argc > 1) {
        size = atoi(argv[1]);
    }
//    Mesh mesh = mesh_create_unit_square(size, 0.0);
//    Mesh mesh = mesh_create_from_file("../../Repos/fvm/data/complex2.msh");
    Mesh mesh = mesh_create_from_file(argv[1]);
    BOUNDARY_CONDITION bcs[] = {
        {bval, 1, BC_DIRICHLET},
        {bval, 2, BC_DIRICHLET},
        {bval, 3, BC_DIRICHLET},
        {bval, 4, BC_DIRICHLET},
    };
    VECTOR* x = vector_create(mesh.ncells);
    LINEAR_SYSTEM ls = linsys_poisson2(mesh, f, bcs, 4);
    blas_solve(ls.A, ls.b, x);
    mesh_write_result_file(mesh, x, "out.vtk");
    linsys_destroy(ls);


//    printf("solving errors\n");
    double err_l1 = 0;
    double err_l2 = 0;
    double ex_l1 = 0;
    double ex_l2 = 0;
    double max_r = 0;
    for(int i=0; i<mesh.ncells; i++) {
        double u_num = vector_get_value(x, i);
        double u_ex = bval( cell_center_point(mesh.cells[i]) );
//        printf("u_num=%lf u_ex=%lf r=%lf\n", u_num, u_ex, fabs(u_num - u_ex));
        double r = u_num - u_ex;
        double area = cell_area(mesh.cells[i]);
        err_l1 += r * area;
        err_l2 += r*r * area;
        ex_l1 += u_ex * area;
        ex_l2 += u_ex*u_ex * area;
        max_r = r > max_r ? r : max_r;
    }
    printf("%d ex_l2=%lf l2=%lf max_r=%lf \n",
            size, sqrt(ex_l2), sqrt(err_l2), max_r);

    vector_destroy(x);
    mesh_destroy(mesh);
    return 0;
}

int stokes(int argc, char* argv[]) {
    int size = 5;
    if (argc > 1)
        size = atoi(argv[1]);
    Mesh mesh = mesh_create_unit_square(size, 0.0);
    LINEAR_SYSTEM ls = linsys_create_stokes(mesh);
    VECTOR *x = vector_create(vector_size(ls.b));
    blas_solve(ls.A, ls.b, x);
    mesh_write_result_stokes(mesh, x, "out.vtk");
    return 0;
}

void fvm_init(int* pargc,char*** pargv) {
#ifdef USING_PETSC
    PetscInitialize(pargc, pargv, NULL, "hehe");
#endif
    srand(time(NULL));
    signal(SIGSEGV, sigsegv_handler);
}

void fvm_finalize() {
#ifdef USING_PETSC
    PetscFinalize();
#endif
}

int main(int argc, char* argv[]) {
    fvm_init(&argc, &argv);
//    int ret = poisson_dt(argc, argv);
    int ret = poisson(argc, argv);
//    int ret = stokes(argc, argv);
    fvm_finalize();
    return ret;
}
