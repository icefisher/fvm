#include "geometry.h"
#include <math.h>
#include <assert.h>
#include <stddef.h>
#include <stdio.h>

POINT point_create(double x, double y, int id) {
    assert(id > -1);
    POINT p;
    p.x = x;
    p.y = y;
    p.id = id;
    return p;
}

double point_distance(POINT p1, POINT p2) {
    return line_length(line_create(&p1, &p2));
}

double point_line_distance(POINT p, LINE l)
{
    double A = l.a->y - l.b->y;
    double B = l.b->x - l.a->x;
    double C = l.a->x*l.b->y - l.b->x*l.a->y;
    double d = fabs(A*p.x + B*p.y + C) / sqrt(A*A+B*B);
    return d;
}

LINE line_create(POINT* p1, POINT* p2) {

    LINE line;
	line.a = p1;
	line.b = p2;
	return line;
}

int line_is_valid(LINE l) {
    return l.a != NULL && l.b != NULL;
}

int line_equals(LINE l1, LINE l2) {
    return (l1.a->id == l2.a->id && l1.b->id == l2.b->id)
            || (l1.a->id == l2.b->id && l1.b->id == l2.a->id);
}

double line_length(LINE l) {
	return sqrt( (l.a->x - l.b->x)*(l.a->x - l.b->x) +  (l.a->y - l.b->y)*(l.a->y - l.b->y) );
}

void line_print(LINE line) {
    printf("line (%.2lf %.2lf) -- (%.2lf %.2lf)\n", line.a->x, line.a->y, line.b->x, line.b->y);
}

POINT line_center_point(LINE l) {
    POINT p = { (l.a->x + l.b->x)/2, (l.a->y + l.b->y)/2, -1 };
    return p;
}


double line_angle(LINE l1, LINE l2)
{
    double A1 = l1.a->y - l1.b->y;
    double B1 = l1.b->x - l1.a->x;
    double A2 = l2.a->y - l2.b->y;
    double B2 = l2.b->x - l2.a->x;
    double cosa = (A1*A2 + B1*B2) / sqrt(A1*A1 + B1*B1) / sqrt(A2*A2+B2*B2);
    return acos(cosa);
}

POINT cell_center_point(CELL cell) {
	int k;
	double x = 0;
	double y = 0;
	for(k=0; k<4; k++) {
		x += cell.points[k]->x;
		y += cell.points[k]->y;
	}
    POINT p = { x/4, y/4, -1 };
	return p;
}

double cell_area(CELL c) {
    /*
     * Вычисляет с помощью полупараллелпипедов
     */
    VEC2D a1 = vec2d_create(*c.points[0], *c.points[1]);
    VEC2D a2 = vec2d_create(*c.points[0], *c.points[3]);
    double cosA = vec2d_scalar(a1, a2) / (vec2d_length(a1) * vec2d_length(a2) );
    double alphaA = acos(cosA);

    VEC2D b1 = vec2d_create(*c.points[2], *c.points[1]);
    VEC2D b2 = vec2d_create(*c.points[2], *c.points[3]);
    double cosB = vec2d_scalar(b1, b2) / (vec2d_length(b1) * vec2d_length(b2) );
    double alphaB = acos(cosB);
    
    double s = 0.5*sin(alphaA)*vec2d_length(a1)*vec2d_length(a2) + 0.5*sin(alphaB)*vec2d_length(b1)*vec2d_length(b2);
    
	return s;
}


CELL cell_create(POINT *p1, POINT *p2, POINT *p3, POINT *p4, int id) {
    assert(id > -1);

    CELL c;
    c.points[0] = p1;
    c.points[1] = p2;
    c.points[2] = p3;
    c.points[3] = p4;
    c.id = id;
    c.lines_marks[0] = -1;
    c.lines_marks[1] = -1;
    c.lines_marks[2] = -1;
    c.lines_marks[3] = -1;
    return c;
}

int cell_is_valid(CELL c) {
    return c.id > -1;
}

/* hot function, must work faster!!! */
LINE cell_common_line(CELL c1, CELL c2) {
    int i,j,k;
    POINT* p[2] = {NULL, NULL};
    k = 0;    
    for(i=0; i<4 && k<2; i++) {
        for(j=0; j<4 && k<2; j++) {
            if (c1.points[i]->id == c2.points[j]->id) {
                p[k++] = c1.points[i];
            }
        }
    }
    return line_create(p[0], p[1]);
}

void cell_get_edges(CELL c, LINE edges[])
{
    edges[0] = line_create(c.points[0], c.points[1]);
    edges[1] = line_create(c.points[1], c.points[2]);
    edges[2] = line_create(c.points[2], c.points[3]);
    edges[3] = line_create(c.points[3], c.points[0]);
}

int cell_has_point(CELL c, POINT *p)
{
    return c.points[0] == p
            || c.points[1] == p
            || c.points[2] == p
            || c.points[3] == p;
}

int cell_has_line(CELL cell, LINE line) {
    if (cell.points[0]->id == line.a->id && cell.points[1]->id == line.b->id)
        return 0;
    if (cell.points[1]->id == line.a->id && cell.points[2]->id == line.b->id)
        return 1;
    if (cell.points[2]->id == line.a->id && cell.points[3]->id == line.b->id)
        return 2;
    if (cell.points[3]->id == line.a->id && cell.points[0]->id == line.b->id)
        return 3;
    //reversed
    if (cell.points[0]->id == line.b->id && cell.points[1]->id == line.a->id)
        return 0;
    if (cell.points[1]->id == line.b->id && cell.points[2]->id == line.a->id)
        return 1;
    if (cell.points[2]->id == line.b->id && cell.points[3]->id == line.a->id)
        return 2;
    if (cell.points[3]->id == line.b->id && cell.points[0]->id == line.a->id)
        return 3;
    return -1;
}

void cell_print(CELL c) {
    int k;
    printf("cell %d ", c.id);
    for(k=0; k<4; k++) {
        if (k > 0)
            printf(" -- ");
        printf("(%.2lf %.2lf)", c.points[k]->x, c.points[k]->y);
    }
    printf("\n");
}

VEC2D vec2d_create(POINT from, POINT to) {
    VEC2D v;
    v.x = (to.x - from.x);
    v.y = (to.y - from.y);
    return v;
}

double vec2d_scalar(VEC2D a, VEC2D b) {
    return a.x*b.x + a.y*b.y;
}

double vec2d_length(VEC2D a) {
    return sqrt(vec2d_scalar(a, a));
}

void vec2d_print(VEC2D v) {
    printf("vec (%.2lf %.2lf)\n", v.x, v.y);
}


