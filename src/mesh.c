#include <stdio.h>
#include "mesh.h"
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "edges.h"

static double mesh_rnd = 0.0;

static double rand_double(double h) {
    double param = mesh_rnd;
	double d = rand();
	double r = d / RAND_MAX;
	return (r*param*2 - param)*h;
}

static void mesh_write_cell_data(Mesh mesh, VECTOR *v, FILE *f)
{
    fprintf(f, "\nCELL_DATA %d\n", mesh.ncells);
    fprintf(f, "SCALARS u float\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.ncells; i++) {
        fprintf(f, "%lf\n", vector_get_value(v, i));
    }
}

static void mesh_write_point_data(Mesh mesh, VECTOR *v, FILE *f)
{
    fprintf(f, "\nPOINT_DATA %d\n", mesh.npoints);
    fprintf(f, "SCALARS u float 1\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.npoints; i++) {
        double mass_center = 0;
        int k = 0;
        for(int j=0; j<mesh.ncells; j++) {
            if (cell_has_point(mesh.cells[j], &mesh.points[i])){
                mass_center += vector_get_value(v, j);
                k++;
            }
        }
        mass_center /= k;
        fprintf(f, "%lf\n", mass_center);
    }
}

/**
 * @brief mesh_create_unit_square создает сетку с рандомными четырехугольными
 *                      ячейками с границами 1,2,3,4 по часовой стрелке начиная с левого края
 * @param size
 * @param cell_random_coef
 * @return
 */
Mesh mesh_create_unit_square(int size, double cell_random_coef) {
    assert(cell_random_coef >= 0.0 && cell_random_coef <= 0.3);

    mesh_rnd = cell_random_coef;

	int ncells = size*size;
	int npoints = (size+1)*(size+1);
	double h = 1.0/size;
	Mesh mesh;
    mesh.cells = (CELL*) malloc(sizeof(CELL)*ncells);
    mesh.points = (POINT*) malloc(sizeof(POINT)*npoints);
	mesh.ncells = ncells;
	mesh.npoints = npoints;
	int i,j,k;
	k = 0;
    for(i=0; i<size+1; i++) {
		for(j=0; j<size+1; j++) {
//            mesh.points[k] = point_create(i*h + rand_double(h) , j*h + rand_double(h), k);
            mesh.points[k] = point_create(j*h + rand_double(h) , i*h + rand_double(h), k);
            k++;
		}
	}
	k = 0;
    LINE edges[4];
    for(i=0; i<size; i++) {
		for(j=0; j<size; j++) {
            mesh.cells[k] = cell_create(
				&mesh.points[i*(size+1) + j],
				&mesh.points[(i+1)*(size+1) + j],
				&mesh.points[(i+1)*(size+1) + j+1],
				&mesh.points[i*(size+1) + j+1],
                k);
            cell_get_edges(mesh.cells[k], edges);
            for(int e=0; e<4; e++)
                edges_add(edges[e], &mesh.cells[k]);

            if (i == 0) {
                mesh.cells[k].lines_marks[3] = 1;
            }
            if (j == 0) {
                mesh.cells[k].lines_marks[0] = 4;
            }
            if (i == size-1) {
                mesh.cells[k].lines_marks[1] = 3;
            }
            if (j == size-1) {
                mesh.cells[k].lines_marks[2] = 2;
            }

            k++;
		}
	}
	return mesh;
}

Mesh mesh_create_from_file(char *filename) {
    FILE* f = fopen(filename, "r");
    if (!f) {
        fprintf(stderr, "Cannot open mesh %s\n", filename);
        exit(-1);
    }
    double a;
    int i,j,cells_idx;
    Mesh mesh;
    fscanf(f, "$MeshFormat %lf %d %d $EndMeshFormat $Nodes %d", &a, &i, &j, &mesh.npoints);
    mesh.points = malloc(sizeof(POINT)*mesh.npoints);
    for(i=0; i<mesh.npoints; i++) {
        POINT p;
        fscanf(f, "%d %lf %lf %lf", &cells_idx, &p.x, &p.y, &a);
        p.id = i;
        mesh.points[i] = p;
    }
    fscanf(f, " $EndNodes $Elements %d", &mesh.ncells);
    mesh.cells = (CELL*) malloc(sizeof(CELL)*mesh.ncells);
    LINE *lines = (LINE*) malloc(sizeof(LINE)*mesh.ncells);
    cells_idx = 0;
    int lines_idx = 0;
    LINE edges[4];
    for(i=0; i<mesh.ncells; i++) {
        int tmp_num, type, ntags, tag;
        fscanf(f, "%d %d %d", &tmp_num, &type, &ntags);
        for(j=0; j<ntags; j++) {
            if (j == 0)
                fscanf(f, "%d", &tag);
            else
                fscanf(f, "%d", &tmp_num);
//            printf("%d\n", tag);
        }
        int np = 0;
        if (type == 1)
            np = 2;
        if (type == 3)
            np = 4;
        int points[4];
        for(j=0; j<np; j++) {
            fscanf(f, "%d", &points[j]);
        }

        if (type == 3) {
            CELL c = cell_create(NULL, NULL, NULL, NULL, cells_idx);
            for(j=0; j<np; j++)
                c.points[j] = &mesh.points[ points[j] - 1 ];
            mesh.cells[cells_idx] = c;

            cell_get_edges(mesh.cells[cells_idx], edges);
            for(int e=0; e<4; e++)
                edges_add(edges[e], &mesh.cells[cells_idx]);

            cells_idx++;
        } else if (type == 1) {
            lines[lines_idx].a =  &mesh.points[ points[0] - 1 ];
            lines[lines_idx].b =  &mesh.points[ points[1] - 1 ];
            lines[lines_idx].mark = tag;
            lines_idx++;
        }
    }
    mesh.ncells = cells_idx;

    //write marks from lines to cells
    int k;
    for(i=0; i<mesh.ncells; i++) {
        for(j=0; j<lines_idx; j++) {
            if ( (k = cell_has_line(mesh.cells[i], lines[j])) != -1) {
                mesh.cells[i].lines_marks[k] = lines[j].mark;
            }
        }
    }
    free(lines);

    fclose(f);
    return mesh;
}

void mesh_destroy(Mesh mesh) {
	free(mesh.points);
	free(mesh.cells);
}

void hehe() {
    int i = 0;
    i = i + 1;
}

void mesh_neighbor_cells(Mesh mesh, CELL c, LINE lines[4], CELL neighbors[4]) {
    int size = mesh.ncells;
    cell_get_edges(c, lines);
    lines[0].mark = c.lines_marks[0];
    lines[1].mark = c.lines_marks[1];
    lines[2].mark = c.lines_marks[2];
    lines[3].mark = c.lines_marks[3];
    neighbors[0].id = -1;
    neighbors[1].id = -1;
    neighbors[2].id = -1;
    neighbors[3].id = -1;
    int k;

    CELL *cells[2];
    int ncells;
    CELL *cell_array[8];
    k = 0;
    ncells = edges_get(lines[0], cells);
    for(int i=0; i<ncells; i++) {
        cell_array[k] = cells[i];
        k++;
    }
    ncells = edges_get(lines[1], cells);
    for(int i=0; i<ncells; i++) {
        cell_array[k] = cells[i];
        k++;
    }
    ncells = edges_get(lines[2], cells);
    for(int i=0; i<ncells; i++) {
        cell_array[k] = cells[i];
        k++;
    }
    ncells = edges_get(lines[3], cells);
    for(int i=0; i<ncells; i++) {
        cell_array[k] = cells[i];
        k++;
    }

//    for(int i=0; i<size; i++) {
//        if (c.id == i)
//            continue;
//        k = 0;
//        hehe();
//        LINE line = cell_common_line(c, mesh.cells[i]);
//        if (line_is_valid(line)) {
//            k = line_equals(lines[0], line) ? 0 : k;
//            k = line_equals(lines[1], line) ? 1 : k;
//            k = line_equals(lines[2], line) ? 2 : k;
//            k = line_equals(lines[3], line) ? 3 : k;
//            neighbors[k] = mesh.cells[i];
//        }
//    }

    ncells = k;
    for(int i=0; i<ncells; i++) {
        if (c.id == cell_array[i]->id)
            continue;
        k = 0;
        LINE line = cell_common_line(c, *cell_array[i]);
        if (line_is_valid(line)) {
            k = line_equals(lines[0], line) ? 0 : k;
            k = line_equals(lines[1], line) ? 1 : k;
            k = line_equals(lines[2], line) ? 2 : k;
            k = line_equals(lines[3], line) ? 3 : k;
            neighbors[k] = *cell_array[i];
        }
    }

}

void mesh_write_mesh(Mesh mesh, FILE* f) {
    fprintf(f, "\
# vtk DataFile Version 2.0\n\
Mesh mozafaka!\n\
ASCII\n\
\n\
DATASET UNSTRUCTURED_GRID\n\
"
    );
    fprintf(f, "\nPOINTS %d float\n", mesh.npoints);
    int i = 0;
    for(i=0; i<mesh.npoints; i++) {
        fprintf(f, "%lf %lf 0.0\n", mesh.points[i].x, mesh.points[i].y);
    }
    fprintf(f, "\nCELLS %d %d\n", mesh.ncells, mesh.ncells*5);
    for(i=0; i<mesh.ncells; i++) {
        fprintf(f, "4 %d %d %d %d\n",
            mesh.cells[i].points[0]->id,
            mesh.cells[i].points[1]->id,
            mesh.cells[i].points[2]->id,
            mesh.cells[i].points[3]->id
            );
    }
    fprintf(f, "\nCELL_TYPES %d\n", mesh.ncells);
    for(i=0; i<mesh.ncells; i++) {
        fprintf(f, "9\n");
    }
}

void mesh_write_result(Mesh mesh, VECTOR *v, FILE *f)
{
    mesh_write_mesh(mesh, f);
    mesh_write_cell_data(mesh, v, f);
//    mesh_write_point_data(mesh, v, f);
}

void mesh_write_result_file(Mesh mesh, VECTOR *v, char *filename)
{
//    printf("writing results\n");
    FILE* f = fopen(filename, "w");
    mesh_write_result(mesh, v, f);
    fclose(f);
}


void mesh_write_result_stokes(Mesh mesh, VECTOR *v, char *filename)
{
    FILE* f = fopen(filename, "w");
    mesh_write_mesh(mesh, f);

    int N = sqrt(mesh.ncells);
//    double h = 1.0/N;
    int offset_u = 0;
    int offset_v = N*(N-1);
    int offset_p = offset_v*2;


    fprintf(f, "\nCELL_DATA %d\n", mesh.ncells);

    fprintf(f, "SCALARS u float\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.ncells; i++) {
        CELL c = mesh.cells[i];
        double u_id = (c.id/N)*(N-1) + c.id % N;
        if ((c.id+1) % N != 0)
            fprintf(f, "%lf\n", vector_get_value(v, offset_u + u_id));
        else
            fprintf(f, "%lf\n", 0.0);
    }

    fprintf(f, "SCALARS v float\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.ncells; i++) {
        CELL c = mesh.cells[i];
        if (c.id < N*(N-1))
            fprintf(f, "%lf\n", vector_get_value(v, offset_v + c.id));
        else
            fprintf(f, "%lf\n", 0.0);
    }


    fprintf(f, "SCALARS p float\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.ncells; i++) {
        fprintf(f, "%lf\n", vector_get_value(v, offset_p + i));
    }

    fprintf(f, "\nPOINT_DATA %d\n", mesh.npoints);

    fprintf(f, "SCALARS p float 1\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.npoints; i++) {
        double mass_center = 0;
        int k = 0;
        for(int j=0; j<mesh.ncells; j++) {
            if (cell_has_point(mesh.cells[j], &mesh.points[i])){
                mass_center += vector_get_value(v, offset_p + j);
                k++;
            }
        }
        mass_center /= k;
        fprintf(f, "%lf\n", mass_center);
    }

    /*
    fprintf(f, "SCALARS vel float 1\n");
    fprintf(f, "LOOKUP_TABLE default\n");
    for(int i=0; i<mesh.npoints; i++) {
        double mass_center = 0;
        int k = 0;
        for(int j=0; j<mesh.ncells; j++) {

            if (cell_has_point(mesh.cells[j], &mesh.points[i])){
                double u_id = (mesh.cells[j].id/N)*(N-1) + mesh.cells[j].id % N;


                mass_center += vector_get_value(v, offset_p + j);
                k++;
            }
        }
        mass_center /= k;
        fprintf(f, "%lf\n", mass_center);
    }*/

    fclose(f);
}
