#pragma once

#include <stdio.h>
#include "geometry.h"
#include "blas.h"

#define MESH_RND 0.3

typedef struct Mesh {
    CELL *cells;
    POINT *points;
	int ncells;
	int npoints;
} Mesh;

void mesh_neighbor_cells(Mesh mesh, CELL p, LINE lines[4], CELL neighbors[4]);
void mesh_write_mesh(Mesh mesh, FILE* f);
void mesh_write_result(Mesh mesh, VECTOR* v, FILE* f);
void mesh_write_result_file(Mesh mesh, VECTOR* v, char* filename);
void mesh_write_result_stokes(Mesh mesh, VECTOR* v, char* filename);
Mesh mesh_create_unit_square(int size, double cell_random_coef);
Mesh mesh_create_from_file(char *filename);
void mesh_destroy(Mesh mesh);

