#pragma once
#include <stdio.h>

#define USING_PETSC
//#define USING_SIMPLE

typedef struct _VECTOR VECTOR;
typedef struct _MATRIX MATRIX;

VECTOR* vector_create(int size);
void vector_destroy(VECTOR *v);
void vector_set_value(VECTOR *v, int i, double value);
double vector_get_value(VECTOR *v, int i);
void vector_copy(VECTOR *src, VECTOR *dst);
void vector_print(VECTOR *v, FILE* f);
int vector_size(VECTOR *v);

MATRIX* matrix_create(int size);
void matrix_destroy(MATRIX *m);
void matrix_set_value(MATRIX *m, int i, int j, double value);
double matrix_get_value(MATRIX *m, int i, int j);
void matrix_print(MATRIX *m, FILE* f);
int matrix_size(MATRIX *m);

void blas_solve(MATRIX *A, VECTOR *b, VECTOR *x);
