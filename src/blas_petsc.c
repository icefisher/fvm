#include "blas.h"

#ifdef USING_PETSC

#include <assert.h>
#include <petscksp.h>


struct _VECTOR {
    int size;
    Vec vector;
};

struct _MATRIX {
    int size;
    Mat matrix;
};

void vector_print(VECTOR *v, FILE *f)
{
    int i;
    for(i=0; i<v->size; i++) {
        fprintf(f, "%.2lf ", vector_get_value(v, i));
    }
    fprintf(f, "\n");
}


void matrix_print(MATRIX *m, FILE *f)
{
    int i,j;
    for(i=0; i<m->size; i++) {
        for(j=0; j<m->size; j++) {
            fprintf(f, "%.2lf ", matrix_get_value(m, i, j));
        }
        fprintf(f, "\n");
    }
}


VECTOR* vector_create(int size)
{
    assert(size > 0);
    VECTOR* v = malloc(sizeof(VECTOR));
    v->size = size;
    VecCreate(PETSC_COMM_WORLD, &v->vector);
    VecSetSizes(v->vector, PETSC_DECIDE, size);
    VecSetUp(v->vector);
    return v;
}


void vector_destroy(VECTOR* v)
{
    VecDestroy(&v->vector);
}


void vector_set_value(VECTOR* v, int i, double value)
{
    assert(i > -1 && i < v->size );

    VecSetValue(v->vector, i, value, INSERT_VALUES);
}


double vector_get_value(VECTOR* v, int i)
{
    assert(i > -1 && i < v->size );
    double val;
    VecGetValues(v->vector, 1, &i, &val);
    return val;
}


MATRIX* matrix_create(int size)
{
    assert(size > 0);

    MATRIX* m = malloc(sizeof(MATRIX));
    m->size = size;
    MatCreate(PETSC_COMM_WORLD, &m->matrix);
    MatSetSizes(m->matrix, PETSC_DECIDE, PETSC_DECIDE, size, size);
    MatSetUp(m->matrix);
    return m;
}


void matrix_destroy(MATRIX* m)
{
    MatDestroy(&m->matrix);
}


void matrix_set_value(MATRIX* m, int i, int j, double value)
{
    assert(i > -1 && i < m->size );
    assert(j > -1 && j < m->size );

    MatSetValue(m->matrix, i, j, value, INSERT_VALUES);
}


double matrix_get_value(MATRIX* m, int i, int j)
{
    assert(i > -1 && i < m->size );
    assert(j > -1 && j < m->size );

    double val;
    MatGetValue(m->matrix, i, j, &val);
    return val;
}

void vector_copy(VECTOR* src, VECTOR* dst)
{
    assert(src->size == dst->size);

    VecCopy(src->vector, dst->vector);
}

int vector_size(VECTOR *v) {
    return v->size;
}

static double ls_tolerance = 1e-10;

void blas_solve(MATRIX *A, VECTOR *b, VECTOR *x) {
//    printf("solving blas\n");
    MatAssemblyBegin(A->matrix, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(A->matrix, MAT_FINAL_ASSEMBLY);
    KSP ksp;
    PC pc;
    KSPCreate(PETSC_COMM_WORLD, &ksp);
    KSPSetOperators(ksp, A->matrix, A->matrix, DIFFERENT_NONZERO_PATTERN);
    KSPGetPC(ksp, &pc);

//    PCSetType(pc, PCILU);
    PCSetType(pc, PCLU);
    KSPSetType(ksp, KSPGMRES);

    KSPSetTolerances(ksp, ls_tolerance, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
    KSPSolve(ksp, b->vector, x->vector);


    int num;
    KSPGetIterationNumber(ksp, &num);

//    printf("iter=%d\n", num);

    KSPDestroy(&ksp);


}

#endif
