#include "blas.h"

#ifdef USING_SIMPLE


#include <assert.h>
#include <string.h>
#include <malloc.h>

struct _VECTOR {
    int size;
    double *data;
};

struct _MATRIX {
    int size;
    double** data;
};

void vector_print(VECTOR *v, FILE *f)
{
    int i;
    for(i=0; i<v->size; i++) {
        fprintf(f, "%.2lf ", v->data[i]);
    }
    fprintf(f, "\n");
}


void matrix_print(MATRIX *m, FILE *f)
{
    int i,j;
    for(i=0; i<m->size; i++) {
        for(j=0; j<m->size; j++) {
            fprintf(f, "%.2lf ", m->data[i][j]);
        }
        fprintf(f, "\n");
    }
}


VECTOR* vector_create(int size)
{
    assert(size > 0);

    VECTOR *v = malloc(sizeof(VECTOR));
    v->size = size;
    v->data = malloc(sizeof(double)*size);
    memset(v->data, 0, sizeof(double)*size);
    return v;
}


void vector_destroy(VECTOR *v)
{
    free(v->data);
    free(v);
}


void vector_set_value(VECTOR *v, int i, double value)
{
    assert(i > -1 && i < v->size );

    v->data[i] = value;
}


double vector_get_value(VECTOR *v, int i)
{
    assert(i > -1 && i < v->size );

    return v->data[i];
}

void vector_copy(VECTOR *src, VECTOR *dst)
{
    assert(src->size == dst->size);

    memcpy(dst->data, src->data, sizeof(double)*src->size);
}

int vector_size(VECTOR *v) {
    return v->size;
}

MATRIX* matrix_create(int size)
{
    assert(size > 0);

    MATRIX *m = malloc(sizeof(MATRIX));
    m->size = size;
    m->data = malloc(sizeof(double*)*size);
    int i;
    for(i=0; i<size; i++) {
        m->data[i] = malloc(sizeof(double)*size);
        memset(m->data[i], 0, sizeof(double)*size);
    }
    return m;
}


void matrix_destroy(MATRIX *m)
{
    int i;
    for(i=0; i<m->size; i++) {
        free(m->data[i]);
    }
    free(m->data);
    free(m);
}


void matrix_set_value(MATRIX *m, int i, int j, double value)
{
    assert(i > -1 && i < m->size );
    assert(j > -1 && j < m->size );

    m->data[i][j] = value;
}


double matrix_get_value(MATRIX *m, int i, int j)
{
    assert(i > -1 && i < m->size );
    assert(j > -1 && j < m->size );

    return m->data[i][j];
}

int matrix_size(MATRIX *m) {
    return m->size;
}

static int ls_max_iteration = 1e3;
static double ls_tolerance = 1e-5;

static void solve_zeidel(MATRIX *A, VECTOR *b, VECTOR *x)
{
    int i,j;
    const int size = vector_size(A);
    VECTOR* x0 = vector_create(size);
    int iter = 0;
    double norm = 0;
    do {
        for(i=0; i<size; i++) {
            double s = vector_get_value(b, i);
            for (j=0; j<size; j++) {
                if (j < i) {
                    s -= matrix_get_value(A, i, j)*vector_get_value(x, j);
                }
                if (j > i) {
                    s -= matrix_get_value(A, i, j)*vector_get_value(x0, j);
                }

            }
            double aii = matrix_get_value(A, i, i);
            vector_set_value(x, i, s/aii);
        }
        norm = 0;
        for(j=0; j<size; j++) {
            double r = fabs(vector_get_value(x, j) - vector_get_value(x0, j));
            if (r > norm)
                norm = r;
        }
        vector_copy(x, x0);
        iter++;
    } while (norm > ls_tolerance && iter < ls_max_iteration);
    vector_destroy(x0);
    fprintf(stderr, "Zeidel iter %d\n", iter);
    if (iter >= ls_max_iteration) {
        fprintf(stderr, "Failed to solve\n");
        exit(-1);
    }
}

static void solve_sor(MATRIX *A, VECTOR *b, VECTOR *x) {
    int i,j;
    const int size = vector_size(A);
    VECTOR *x0 = vector_create(size);
    int iter = 0;
    double norm = 0;
    const double omega = 1.5;
    do {
        for(i=0; i<size; i++) {
            double s = vector_get_value(b, i);
            for (j=0; j<size; j++) {
                if (j < i) {
                    s -= matrix_get_value(A, i, j)*vector_get_value(x, j);
                }
                if (j > i) {
                    s -= matrix_get_value(A, i, j)*vector_get_value(x0, j);
                }

            }
            double aii = matrix_get_value(A, i, i);
            vector_set_value(x, i,
                             omega*s/aii + (1-omega)*vector_get_value(x0, i));
        }
        norm = 0;
        for(j=0; j<size; j++) {
            double r = fabs(vector_get_value(x, j) - vector_get_value(x0, j));
            if (r > norm)
                norm = r;
        }
        vector_copy(x, x0);
        iter++;
    } while (norm > ls_tolerance && iter < ls_max_iteration);
    vector_destroy(x0);
    fprintf(stderr, "SOR iter %d\n", iter);
    if (iter >= ls_max_iteration) {
        fprintf(stderr, "Failed to solve\n");
        exit(-1);
    }
}

void blas_solve(MATRIX *A, VECTOR *b, VECTOR *x) {
    matrix_print(A, stdout);
    vector_print(b, stdout);
    solve_sor(A, b, x);
}

#endif
