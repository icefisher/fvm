#include <string.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>

#include "mesh.h"
#include "solver.h"

static double _tau = 1;
static double _crho = 1;
static VECTOR* _x0;
static double _u = 0;

LINEAR_SYSTEM linsys_create_poisson(Mesh mesh, double lambda, double (*f)(POINT), BOUNDARY_CONDITION *bcs, int nbcs) {
    CELL cells[4];
    LINE lines[4];
    double other_val, center_val, boundary_val;
    MATRIX* A = matrix_create(mesh.ncells);
    VECTOR* b = vector_create(mesh.ncells);
    const int size = vector_size(b);
    for(int i=0; i<size; i++) {
        CELL c = mesh.cells[i];
        mesh_neighbor_cells(mesh, c, lines, cells);
        center_val = 0;
        boundary_val = 0;
        POINT center = cell_center_point(c);
        for(int j=0; j<4; j++) {
            POINT lp;
            double l = line_length(lines[j]);
            if (cell_is_valid(cells[j])) {
                lp = cell_center_point(cells[j]);
            } else {
                lp = line_center_point(lines[j]);

            }
            other_val = l / point_distance(lp, center)*lambda;


            if (cell_is_valid(cells[j])) {
                assert(lines[j].mark == -1);
                matrix_set_value(A, c.id, cells[j].id, other_val);
                center_val += other_val;
            } else {
                for(int k=0; k<nbcs; k++) {
                    if (bcs[k].mark == lines[j].mark) {
                        if (bcs[k].type == BC_DIRICHLET) {
                            boundary_val += bcs[k].fun(lp)*other_val;
                            center_val += other_val;
                        } else if (bcs[k].type == BC_NEUMANN) {
                            boundary_val -= bcs[k].fun(lp)*l; //minus - because normal is always outside
                        }
                        break;
                    }
                }
            }

        }
        matrix_set_value(A, c.id, c.id, -center_val - _crho/_tau*cell_area(c));
        vector_set_value(b, c.id, -boundary_val - (f(center) + vector_get_value(_x0, c.id)*_crho/_tau )*cell_area(c));
    }
    LINEAR_SYSTEM ls;
    ls.mesh = mesh;
    ls.A = A;
    ls.b = b;
    ls.max_iteration = 1000;
    ls.print_norm = 0;
    ls.sor_omega = 1.5;
    ls.tolerance = 1e-5;
    return ls;
}

LINEAR_SYSTEM linsys_poisson2(Mesh mesh, double (*f)(POINT), BOUNDARY_CONDITION *bcs, int nbcs) {
//    printf("assembling system\n");
    CELL cells[4];
    LINE lines[4];
    double other_val, center_val, boundary_val;
    MATRIX* A = matrix_create(mesh.ncells);
    VECTOR* b = vector_create(mesh.ncells);
    const int size = vector_size(b);
    for(int i=0; i<size; i++) {
        CELL c = mesh.cells[i];
        mesh_neighbor_cells(mesh, c, lines, cells);
        center_val = 0;
        boundary_val = 0;
        POINT center = cell_center_point(c);
        for(int j=0; j<4; j++) {
            POINT lp;
            double l = line_length(lines[j]);
            if (cell_is_valid(cells[j])) {
                lp = cell_center_point(cells[j]);
            } else {
                lp = line_center_point(lines[j]);
            }
            //spobos 1
            other_val = l / point_distance(lp, center);
            //sposob 2
//            double h1 = point_line_distance(lp, lines[j]);
//            double h2 = point_line_distance(center, lines[j]);
//            other_val = l/(h1+h2);
            //sposob 3
//            LINE hehe = line_create(&lp, &center);
//            double alpha = line_angle(lines[j], hehe);
//            other_val = l * sin(alpha) / point_distance(lp, center);
            //sposob 4
//            double h1 = point_line_distance(lp, lines[j]);
//            double h2 = point_line_distance(center, lines[j]);

////            if (h1 > 0 && h2 > 0)
////                other_val = l * (h1+h2) / (4*h1*h2);
////            else if (h1 > 0)
////                other_val = l / h1;
////            else if (h2 > 0)
////                other_val = l / h2;

//            other_val = l / (h1 + h2);

            if (cell_is_valid(cells[j])) {
                assert(lines[j].mark == -1);
                matrix_set_value(A, c.id, cells[j].id, other_val);
                center_val += other_val;
            } else {
                for(int k=0; k<nbcs; k++) {
                    if (bcs[k].mark == lines[j].mark) {
                        if (bcs[k].type == BC_DIRICHLET) {
                            boundary_val += bcs[k].fun(lp)*other_val;
                            center_val += other_val;
                        } else if (bcs[k].type == BC_NEUMANN) {
                            boundary_val -= bcs[k].fun(lp)*l; //minus - because normal is always outside
                        }
                        break;
                    }
                }
            }

        }
        matrix_set_value(A, c.id, c.id, -center_val);
        vector_set_value(b, c.id, -boundary_val - f(center)*cell_area(c));
    }
    LINEAR_SYSTEM ls;
    ls.mesh = mesh;
    ls.A = A;
    ls.b = b;
    ls.max_iteration = 1000;
    ls.print_norm = 0;
    ls.sor_omega = 1.5;
    ls.tolerance = 1e-5;
    return ls;
}

void linsys_destroy(LINEAR_SYSTEM ls) {
    matrix_destroy(ls.A);
    vector_destroy(ls.b);
}

LINEAR_SYSTEM linsys_create_unstationary_poisson(Mesh mesh, VECTOR* x0, double tau, double lambda, double (*f)(POINT), BOUNDARY_CONDITION *bcs, int nbcs)
{
    _x0 = x0;
    _tau = tau;
    _crho = 1;
    _u = 1;

    LINEAR_SYSTEM ls = linsys_create_poisson(mesh, lambda, f, bcs, nbcs);
    return ls;
}

typedef struct {
    int p,w,e,s,n;
} ID;

int valid_u(CELL c, int N) {
    return (c.id + 1) % N != 0;
}
int left_u(CELL c, int N) {
    return c.id % N == 0;
}
int right_u(CELL c, int N) {
    return (c.id + 2) % N == 0;
}
int top_u(CELL c, int N) {
    return c.id >= N*(N-1);
}
int bottom_u(CELL c, int N) {
    return c.id < N;
}

int valid_v(CELL c, int N){
    return c.id < N*(N-1);
}

int top_v(CELL c, int N){
    return c.id >= N*(N-2);
}

int bottom_v(CELL c, int N){
    return c.id < N;
}
int left_v(CELL c, int N) {
    return c.id % N == 0;
}
int right_v(CELL c, int N) {
    return (c.id + 1) % N == 0;
}

LINEAR_SYSTEM linsys_create_stokes(Mesh mesh)
{
    ID u,v,p;
    int N = sqrt(mesh.ncells);
    double h = 1.0/N;
    int offset_u = 0;
    int offset_v = N*(N-1);
    int offset_p = offset_v*2;
    MATRIX *A = matrix_create(offset_p + mesh.ncells);
    VECTOR *b = vector_create(offset_p + mesh.ncells);
    for(int i=0; i<mesh.ncells; i++) {
        CELL c = mesh.cells[i];
        u.p = (c.id/N)*(N-1) + c.id % N;
        u.w = u.p - 1;
        u.e = u.p + 1;
        u.n = u.p + (N-1);
        u.s = u.p - (N-1);

        v.p = c.id;
        v.s = v.p - N;
        v.n = v.p + N;
        v.w = v.p - 1;
        v.e = v.p + 1;

        p.p = c.id;
        p.e = p.p + 1;
        p.n = p.p + N;

        if (valid_u(c, N)) {
            matrix_set_value(A, u.p, offset_u + u.p, 4);
            matrix_set_value(A, u.p, offset_p + p.e, h);
            matrix_set_value(A, u.p, offset_p + p.p, -h);
            if (!left_u(c, N)) {
                matrix_set_value(A, u.p, offset_u + u.w, -1);
            }
            if (!right_u(c, N)) {
                matrix_set_value(A, u.p, offset_u + u.e, -1);
            }
            if (!top_u(c, N)) {
                matrix_set_value(A, u.p, offset_u + u.n, -1);
            } else {
                matrix_set_value(A, u.p, offset_u + u.p, 5);
                vector_set_value(b, u.p, 2);
            }
            if (!bottom_u(c, N)) {
                matrix_set_value(A, u.p, offset_u + u.s, -1);
            }
        }

        if (valid_v(c, N)) {
            matrix_set_value(A, offset_v + v.p, offset_v + v.p, 4);
            matrix_set_value(A, offset_v + v.p, offset_p + p.n, h);
            matrix_set_value(A, offset_v + v.p, offset_p + p.p, -h);
            if (!top_v(c, N)) {
                matrix_set_value(A, offset_v + v.p, offset_v + v.n, -1);
            }
            if (!bottom_v(c, N)) {
                matrix_set_value(A, offset_v + v.p, offset_v + v.s, -1);
            }
            if (!left_v(c, N)) {
                matrix_set_value(A, offset_v + v.p, offset_v + v.w, -1);
            }
            if (!right_v(c, N)) {
                matrix_set_value(A, offset_v + v.p, offset_v + v.e, -1);
            }
        }

        if ((c.id+1) % N != 0)
            matrix_set_value(A, offset_p + p.p, u.p, 1);
        if ((c.id) % N != 0)
            matrix_set_value(A, offset_p + p.p, u.w, -1);
        if (c.id < N*(N-1))
            matrix_set_value(A, offset_p + p.p, offset_v + v.p, 1);
        if (c.id >= N)
            matrix_set_value(A, offset_p + p.p, offset_v + v.s, -1);
    }

    LINEAR_SYSTEM ls;
    ls.mesh = mesh;
    ls.A = A;
    ls.b = b;
    ls.max_iteration = 1000;
    ls.print_norm = 0;
    ls.sor_omega = 1.5;
    ls.tolerance = 1e-5;
    return ls;
}


