sources=main.c mesh.c geometry.c solver.c blas.c blasli.c
appname=fvm
LIBS=-L/opt/lis/lib -llis -lm 
#LIBS=-lm
INCLUDE=-isystem/opt/lis/include
CFLAGS_DEBUG=-pg -g -Wall -Wpedantic -Wno-unused-variable -std=c99
CFLAGS_RELEASE=-O3 -Wno-unused-variable -std=c99

all: debug

bin:
	mkdir -p bin

$(appname): bin bin/$(appname)

bin/$(appname): $(patsubst %.c, bin/%.o, $(sources))
	gcc $^ $(CFLAGS) -o bin/$(appname) $(LIBS)

bin/%.o: src/%.c
	gcc $(INCLUDE) $(CFLAGS) -c $^ -o $@

clean:
	rm -rf bin *.vtk

rebuild: clean $(appname)

paraview: rebuild
	bin/$(appname) 20
	paraview out.vtk

debug-flags:
	$(eval CFLAGS:=$(CFLAGS_DEBUG))

release-flags:
	$(eval CFLAGS:=$(CFLAGS_RELEASE))

debug: debug-flags $(appname)
release: release-flags $(appname)
